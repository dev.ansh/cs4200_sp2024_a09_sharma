# Devansh Sharma

## PhD in Data Science

## Part 1

### How long it takes to complete this assignment?
- About 3 hours

### What is the most challenging part for you?
- English to FOL conversion

### What are the differences between FOL and propositional logic? 
- First-order logic (FOL) includes quantifiers and can express relationships about objects, unlike propositional logic which only handles fixed propositions without internal structure. FOL is more expressive but also more complex computationally.

### We have discussed syntax and semantics of FOL. The next step to conduct inference. Do some research and list a few inference methods for FOL.
- In First-Order Logic, inference is the process used to derive new facts from known ones. Key methods include:

    - **Universal Generalization**: Inferring that what is true for some arbitrary instance holds universally.
    - **Universal Instantiation**: Deducing specific instances from a general universal statement.
    - **Existential Instantiation**: Inferring a concrete example from a statement that asserts existence.
